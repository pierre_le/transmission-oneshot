# Transmission one-shot container

## Usage

When the ephemeral server is ready (or inside a VPS initialization script), execute this:

```
#!/bin/bash
TORRENT_BIN_PREFIX="<URL_TO_TORRENTS_LIST>"
curl 'https://bitbucket.org/pierre_le/transmission-oneshot/raw/master/auto-deploy.sh' | bash
```

This script will automatically install all required dependencies, deploy the project and run the container.
When it's done, access the Transmission UI at:

```
http://<server_ip>:9091/
```

## Locations

- `/downloads`: finished downloads

- `/config`: Transmission configuration files

- `/watch`: Torrent files that are dropped into that folder will be automatically added and started.

## Caution

This container is meant to be used in an ephemeral server, that will be destroyed in the short-term.
Do *NOT* use this in a "sedentary" server, as it misses a lot of features that would be needed to secure this deployment (the first being the lack of HTTPS).
