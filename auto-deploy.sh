#!/bin/bash
#   Use this script as the initialization script when setting up a new VPS.
#   MAKE SURE YOU'VE SET `TORRENT_BIN_PREFIX` BEFOREHAND.
#
set -e

# Install Docker + Compose

apt-get update -q -y
apt-get -q -y install \
    apt-transport-https ca-certificates \
    curl wget \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update -q -y
apt-get install -y -q docker-ce
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Pull the container
apt-get install -q -y git
git clone -q https://bitbucket.org/pierre_le/transmission-oneshot.git
cd transmission-oneshot

# Retrieve any torrent currently in wait queue
#TORRENTS_LIST=`curl "$TORRENT_BIN_PREFIX/" | grep -oP 'title="\K([a-zA-Z0-9\-\._\+]+\.torrent)'`
#cd watch
#for file in $TORRENTS_LIST
#do
#    FULL_URL="$TORRENT_BIN_PREFIX/$file"
#    echo $FULL_URL
#    (wget $FULL_URL && echo "[ok] Fetched new torrent: $file ($FULL_URL)") || echo "[!] Failed to fetch torrent: $file ($FULL_URL)"
#done
#cd -

# Ignition
docker-compose up -d

# All done
IP=`ip a | grep -E 'inet.+eth0' | head -1 | sed -E 's/ +inet ([0-9\.]+).+/\1/'`
echo "**************************************************"
echo "You can now browse to: http://$IP:9091/"
echo "**************************************************"
